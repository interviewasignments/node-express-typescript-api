import app from './../../src/app';
import {expect} from 'chai';
import {agent as request} from 'supertest';
import 'mocha';

describe('Parse API (POST): /api/v1/parse =>', () => {
    it('should return success 200 response', async() => {
        const res = await request(app).post('/api/v1/parse').send({data: 'JOHN0000MICHAEL0009994567'});
        expect(res.status).to.equal(200);
    })

    it('should return success 200 response with data', async() => {
        const res = await request(app).post('/api/v1/parse').send({data: 'JOHN0000MICHAEL0009994567'});   
        expect(res.body).not.to.be.empty;
    })

    it('should return Failure 400 response with bad data', async() => {
        const res = await request(app).post('/api/v1/parse').send({data: 'JOHN0000MICHAEL009994567'});
        expect(res.status).to.equal(400);
    })

    it('should return Failure 400 response with empty data', async() => {
        const res = await request(app).post('/api/v1/parse/').send({data: ''});
        expect(res.status).to.equal(400);
    })

    it('should return Failure 404 with wrong url', async() => {
        const res = await request(app).post('/api/v1/pars/').send({data: ''});
        expect(res.status).to.equal(404);
    })

})

describe('Parse API (POST): /api/v2/parse =>', () => {
    it('should return success 200 response', async() => {
        const res = await request(app).post('/api/v2/parse').send({data: 'JOHN0000MICHAEL0009994567'});
        expect(res.status).to.equal(200);
    })

    it('should return success 200 response with data', async() => {
        const res = await request(app).post('/api/v2/parse').send({data: 'JOHN0000MICHAEL0009994567'});   
        expect(res.body).not.to.be.empty;
    })

    it('should return Failure 400 response with bad data', async() => {
        const res = await request(app).post('/api/v2/parse').send({data: 'JOHN0000MICHAEL009994567'});
        expect(res.status).to.equal(400);
    })

    it('should return Failure 400 response with empty data', async() => {
        const res = await request(app).post('/api/v2/parse/').send({data: ''});
        expect(res.status).to.equal(400);
    })

    it('should return Failure 404 with wrong url', async() => {
        const res = await request(app).post('/api/v2/pars/').send({data: ''});
        expect(res.status).to.equal(404);
    })
})