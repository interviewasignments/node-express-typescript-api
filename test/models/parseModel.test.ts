import {expect} from 'chai';
import {ParseModel} from '../../src/models/parseModel';
import { ParseOutput } from '../../src/interfaces/parseInterface';

describe("ParseModel - convertStringToJson1 () :", () => {
    let parseModel = new ParseModel();
    it('should return json1 object on successful', function () {
        const output:ParseOutput | boolean = parseModel.convertStringToJson1("JOHN0000MICHAEL0009994567");
        expect(output).to.have.property('firstName');
        expect(output).to.have.property('lastName');
        expect(output).to.have.property('clientId');
    });

    it('should return json1 object values on successful', function () {
        const output:any = parseModel.convertStringToJson1("JOHN0000MICHAEL0009994567");
        expect(output.firstName).to.equal('JOHN0000');
        expect(output.lastName).to.equal('MICHAEL000');
        expect(output.clientId).to.equal('9994567');
    });

    it('should return False value on failure', function () {
        const output:ParseOutput | boolean = parseModel.convertStringToJson1("JOHNMICHAEL9994567");
        expect(output).to.equal(false);
    });
});

describe("ParseModel - convertStringToJson2 () :", () => {
    let parseModel = new ParseModel();
    it('should return json1 object on successful', function () {
        const output:ParseOutput | boolean = parseModel.convertStringToJson2("JOHN0000MICHAEL0009994567");
        expect(output).to.have.property('firstName');
        expect(output).to.have.property('lastName');
        expect(output).to.have.property('clientId');
    });

    it('should return json1 object values on successful', function () {
        const output:any = parseModel.convertStringToJson2("JOHN0000MICHAEL0009994567");
        expect(output.firstName).to.equal('JOHN');
        expect(output.lastName).to.equal('MICHAEL');
        expect(output.clientId).to.equal('999-4567');
    });

    it('should return False value on failure', function () {
        const output:ParseOutput | boolean = parseModel.convertStringToJson2("JOHNMICHAEL9994567");
        expect(output).to.equal(false);
    });
});