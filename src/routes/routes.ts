import {Request, Response} from "express";
import { ParseController } from "../controllers/parseController";
import express from 'express';
export class Routes {

    public parser= new ParseController();
    public routes(app: express.Application): void {
        app.route('/')
        .get((req: Request, res: Response) => {
            res.status(200).send({
                message: 'GET request successfulll!!!!'
            })
        })

        // v1 Parser
        app.route('/api/v1/parse')
        // POST endpoint
        .post((req: Request, res: Response) => {
            return this.parser.parseToJson1(req, res);
        })

        // v2 Parser
        app.route('/api/v2/parse')
        // POST endpoint
        .post((req: Request, res: Response) => {
            return this.parser.parseToJson2(req, res);
        })
    }
}