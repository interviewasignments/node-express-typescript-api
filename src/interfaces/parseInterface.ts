export interface ParseOutput{
    firstName: string,
    lastName: string,
    clientId: string
}

export interface ParseInput{
    data: string
}