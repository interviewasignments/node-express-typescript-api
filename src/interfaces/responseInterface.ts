import { ParseOutput } from "./parseInterface";

export interface ResponseObject{
    statusCode: number,
    data?: ParseOutput,
    error?: string
}