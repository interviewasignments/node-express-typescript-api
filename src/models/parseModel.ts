
import { ParseOutput, ParseInput } from "../interfaces/parseInterface";

export class ParseModel{

    public convertStringToJson1(value:string):ParseOutput | boolean {
        try{
            const values=value.split("000");
            const parseData:ParseOutput={
                firstName: values[0]+"0000",
                lastName: values[1].substring(1)+"000",
                clientId: values[2]
            }
            return parseData;
        }catch{
            return false;
        }

    }

    public convertStringToJson2(value:string): ParseOutput | boolean {
        try{
            const values=value.split("000");
            const parseData:ParseOutput={
                firstName: values[0],
                lastName: values[1].substring(1),
                clientId: values[2].substring(0,3)+'-'+values[2].substring(3)
            }
            return parseData;
        }catch{
            return false;
        }
    }
}