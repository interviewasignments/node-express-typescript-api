import { Request, Response } from 'express';
import { ParseModel } from '../models/parseModel';
import { ParseInput, ParseOutput } from '../interfaces/parseInterface';
import { BadRequest, ConversionMessage } from '../constants/errorMessages';
import { ResponseObject } from '../interfaces/responseInterface';

export class ParseController{
    public parseModel = new ParseModel();
    
    public parseToJson1(req: Request, res: Response): ResponseObject {
        const payload : ParseInput = req.body;
        if(payload.data && payload.data.indexOf("0000") === -1 || payload.data.split("000").length !==3){
            return res.status(400).json({statusCode:400, error:BadRequest});
        }{
            const convertedObject: ParseOutput | boolean = this.parseModel.convertStringToJson1(payload.data);
            if(convertedObject){
                return res.status(200).json({statusCode:200, data:convertedObject});
            }else{
                return res.status(500).json({statusCode:500, error:ConversionMessage});
            }
        }

    }

    public parseToJson2(req: Request, res: Response): ResponseObject{
        const payload = req.body;
        if(payload.data && payload.data.indexOf("0000") === -1 || payload.data.split("000").length !==3){
            return res.status(400).json({statusCode:400, data:BadRequest});
        }{
            const convertedObject: ParseOutput | boolean = this.parseModel.convertStringToJson2(payload.data);
            if(convertedObject){
                return res.status(200).json({statusCode:200, data:convertedObject});
            }else{
                return res.status(500).json({statusCode:500, error:ConversionMessage});
            }
        }

    }

}