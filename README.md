# node-express-typescript-api


## How to Run this Project

### Clone this Repository as below

`git clone https://gitlab.com/interviewasignments/node-express-typescript-api.git`


### Check for Linting for static code analysis using

`npm run prebuild`


### Build the project using

`npm run build`


### Run the project using

`npm start`


### Test the Project using

`npm test`


## Node.js Project Requirements
### Technology Stack
#### • Node.js
#### • TypeScript
#### • Express
### Functionality and Design

``` 
The application must expose restful endpoints that will parse data (passed in the request body) and return the value back to the
client. The API will have two versions and depending on the version endpoint, the parsing of the data will return a different value
back to the client. Use TypeScript interfaces so the code assumes the design / object properties.

```

### Endpoints

`/api/v1/parse (POST)`

`/api/v2/parse (POST)`

### Request Body
```
{
 data: “JOHN0000MICHAEL0009994567”
}

```

### Expected Results

`/api/v1/parse – Response Body`

```
{
 statusCode: 200,
 data: {
    firstName: “JOHN0000”,
    lastName: “MICHAEL000”,
    clientId: “9994567”
 }
}
```


`/api/v2/parse – Response Body`

```
{
 statusCode: 200,
 data: {
    firstName: “JOHN”,
    lastName: “MICHAEL”,
    clientId: “999-4567”
 }
}
```


## Technical Notes

### Assumptions Made

``` Request Body is JSON having data attribute as formatted string “JOHN0000MICHAEL0009994567”

    From the above input string we have 3 different attributes to be pulled out.
    1. First Name (Which is the starting part of string like:  First Name - {0000}).
    2. Last Name  (Which is the middle part of the string: {0000}- Last Name -{000}).
    3. Client Id  (Which is the last part of the string: {000}- Client Id).

    As per the above requirement and based on the format provided the following analysis and technical decisions are taken.
```
### Combination Looks

`firstName0000lastName000clientId`

### Analysis

``` Once the request is made with url : /api/v1/parse the output is the converted format of the above mentioned string to a proper json object.

    I can achieve this through multiple ways:
    1. I can perform *String* operations available in javascript such as *split(), substring()*.
    2. I can perform *Regex* patterns to match the desired format from the given string.
    3. I can perform *Iterative* operations such as For loop and then followed by If Conditions.
```

### Approach Taken
```
    Since I have 3 options described as above , the option 1 is chosen by me.
    Why in my Opinion:
    1. Option 3 will be expensive as we will be deleaing with loops and also code looks lengthy to have a desired output.
    2. Option 2 will be complex when regex pattern goes lengthy with multiple constraints and need to habdle multiple scenarios.
    3. Option 1 seems simple and easily understandable by peer developers to review even if they are not aware of the requirements.
```

### Technology Used

```
    I have used Node JS, Express Js, TypeScript to build API.
    
    I have used Chai, SuperTest for Unit and Integration testing.

    I have used TSLint for Static Code analysis.

    I have used VisualCode Studio as Editor.

    I have used Gitlab as Source control.

```    




